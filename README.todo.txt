
- don't allow upgrades across major versions?
https://docs.gitlab.com/omnibus/update/README.html
https://docs.gitlab.com/ce/policy/maintenance.html#upgrade-recommendations
https://docs.ansible.com/ansible/latest/user_guide/playbooks_tests.html#comparing-versions

- letsencrypt integration

- enable incoming email?
https://docs.gitlab.com/ce/administration/incoming_email.html

- enable and explore Mattermost
https://docs.gitlab.com/omnibus/gitlab-mattermost

- explore Terraform integration?
https://docs.gitlab.com/ce/user/infrastructure/#infrastructure-as-code
https://docs.gitlab.com/ce/administration/terraform_state
