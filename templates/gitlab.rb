
external_url '{{ gitlab_external_url }}'

gitlab_rails['initial_root_password'] = "{{ gitlab_initial_root_password }}"

nginx['redirect_http_to_https'] = true
nginx['ssl_certificate'] = "{{ gitlab_ssl_cert }}"
nginx['ssl_certificate_key'] = "{{ gitlab_ssl_key }}"

gitlab_rails['smtp_enable'] = {{ gitlab_smtp_enable | bool | lower }}
gitlab_rails['smtp_address'] = '{{ gitlab_smtp_address }}'
gitlab_rails['smtp_port'] = {{ gitlab_smtp_port }}
gitlab_rails['smtp_user_name'] = '{{ gitlab_smtp_user_name }}'
gitlab_rails['smtp_password'] = '{{ gitlab_smtp_password }}'
gitlab_rails['smtp_domain'] = '{{ gitlab_smtp_domain }}'
gitlab_rails['smtp_authentication'] = '{{ gitlab_smtp_authentication }}'
gitlab_rails['smtp_enable_starttls_auto'] = {{ gitlab_smtp_enable_starttls_auto | bool | lower }}
gitlab_rails['smtp_tls'] = {{ gitlab_smtp_tls | bool | lower }}

### Email settings
gitlab_rails['gitlab_email_enabled'] = {{ gitlab_email_enabled | bool | lower }}
gitlab_rails['gitlab_email_from'] = '{{ gitlab_email_from }}'
gitlab_rails['gitlab_email_display_name'] = '{{ gitlab_email_display_name }}'
gitlab_rails['gitlab_email_reply_to'] = '{{ gitlab_email_reply_to }}'
gitlab_rails['gitlab_email_subject_suffix'] = '{{ gitlab_email_subject_suffix }}'
gitlab_rails['gitlab_email_smime_enabled'] = false
# gitlab_rails['gitlab_email_smime_key_file'] = '/etc/gitlab/ssl/gitlab_smime.key'
# gitlab_rails['gitlab_email_smime_cert_file'] = '/etc/gitlab/ssl/gitlab_smime.crt'
# gitlab_rails['gitlab_email_smime_ca_certs_file'] = '/etc/gitlab/ssl/gitlab_smime_cas.crt'

### GitLab user privileges
gitlab_rails['gitlab_default_can_create_group'] = {{ gitlab_default_can_create_group | bool | lower }}
gitlab_rails['gitlab_username_changing_enabled'] = {{ gitlab_username_changing_enabled | bool | lower }}

### Default Theme
### Available values:
##! `1`  for Indigo
##! `2`  for Dark
##! `3`  for Light
##! `4`  for Blue
##! `5`  for Green
##! `6`  for Light Indigo
##! `7`  for Light Blue
##! `8`  for Light Green
##! `9`  for Red
##! `10` for Light Red
gitlab_rails['gitlab_default_theme'] = {{ gitlab_default_theme }}

### Default project feature settings
gitlab_rails['gitlab_default_projects_features_issues'] = {{ gitlab_default_projects_features_issues | bool | lower }}
gitlab_rails['gitlab_default_projects_features_merge_requests'] = {{ gitlab_default_projects_features_merge_requests | bool | lower }}
gitlab_rails['gitlab_default_projects_features_wiki'] = {{ gitlab_default_projects_features_wiki | bool | lower }}
gitlab_rails['gitlab_default_projects_features_snippets'] = {{ gitlab_default_projects_features_snippets | bool | lower }}
gitlab_rails['gitlab_default_projects_features_builds'] = {{ gitlab_default_projects_features_builds | bool | lower }}
gitlab_rails['gitlab_default_projects_features_container_registry'] = {{ gitlab_default_projects_features_container_registry | bool | lower }}

### Incoming email
gitlab_rails['incoming_email_enabled'] = false

### Terraform state
gitlab_rails['terraform_state_enabled'] = false

# Let's Encrypt integration
################################################################################
letsencrypt['enable'] = false
# letsencrypt['contact_emails'] = [] # This should be an array of email addresses to add as contacts
# letsencrypt['group'] = 'root'
# letsencrypt['key_size'] = 2048
# letsencrypt['owner'] = 'root'
# letsencrypt['wwwroot'] = '/var/opt/gitlab/nginx/www'
# See http://docs.gitlab.com/omnibus/settings/ssl.html#automatic-renewal for more on these sesttings
# letsencrypt['auto_renew'] = true
# letsencrypt['auto_renew_hour'] = 0
# letsencrypt['auto_renew_minute'] = nil # Should be a number or cron expression, if specified.
# letsencrypt['auto_renew_day_of_month'] = "*/4"


# To change other settings, see:
# https://gitlab.com/gitlab-org/omnibus-gitlab/blob/master/README.md#changing-gitlab-yml-settings

### OmniAuth Settings
###! Docs: https://docs.gitlab.com/ee/integration/omniauth.html
# gitlab_rails['omniauth_enabled'] = nil
# gitlab_rails['omniauth_allow_single_sign_on'] = ['saml']
# gitlab_rails['omniauth_sync_email_from_provider'] = 'saml'
# gitlab_rails['omniauth_sync_profile_from_provider'] = ['saml']
# gitlab_rails['omniauth_sync_profile_attributes'] = ['email']
# gitlab_rails['omniauth_auto_sign_in_with_provider'] = 'saml'
# gitlab_rails['omniauth_block_auto_created_users'] = true
# gitlab_rails['omniauth_auto_link_ldap_user'] = false
# gitlab_rails['omniauth_auto_link_saml_user'] = false
# gitlab_rails['omniauth_auto_link_user'] = ['saml']
# gitlab_rails['omniauth_external_providers'] = ['twitter', 'google_oauth2']
# gitlab_rails['omniauth_allow_bypass_two_factor'] = ['google_oauth2']


{% if gitlab_cas_url != false %}
gitlab_rails['omniauth_allow_single_sign_on'] = ['cas3']
gitlab_rails['omniauth_auto_sign_in_with_provider'] = 'cas3'
# gitlab_rails['block_auto_created_users'] = false
gitlab_rails['omniauth_auto_link_user'] = ["cas3"]
gitlab_rails['omniauth_block_auto_created_users'] = false
gitlab_rails['omniauth_providers'] = [{
	"name"=> "cas3",
	"label"=> "cas",
	"args"=> {
		"url"=> '{{ gitlab_cas_url }}',
		"login_url"=> '/login',
		"service_validate_url"=> '/p3/serviceValidate',
		"logout_url"=> '/logout'
	}
}]
{% endif %}
