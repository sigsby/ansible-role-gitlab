
# Gitlab Ansible Role

This project is an opinionated Ansible role for deployment of a Gitlab server.

Notable opinions include:
- Most features are disabled/locked-down by default
- Single Sign-On can be enabled using CAS

Variables of particular note:
- `gitlab_initial_root_password`: this is set to a random password by default.
- `gitlab_cas_url`: set this to a CAS server base url to activate CAS SSO

Also see:
- `vars/main.yml:gitlab_settings` for default runtime settings when using this role.
- `defaults/main.yml:gitlab_settings_override` for a full list of possible runtime settings.
- `files/gitlab.default.rb` for the default gitlab config file from a clean install
